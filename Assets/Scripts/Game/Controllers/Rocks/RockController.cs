﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RockController : MonoBehaviour
{
    public float speed = 100f;

    Rect left, right, top, bottom;

    Camera cam;

    public float onHitDamage;

    void Start()
    {
        cam = Camera.main;

        var screenBottomLeft = cam.ViewportToWorldPoint(new Vector3(0, 0, transform.position.z));
        var screenTopRight = cam.ViewportToWorldPoint(new Vector3(1, 1, transform.position.z));

        float screenWidth = screenTopRight.x - screenBottomLeft.x;
        float screenHeight = screenTopRight.y - screenBottomLeft.y;

        transform.localScale = new Vector3(Random.Range(10f, 40f), Random.Range(10f, 40f), Random.Range(10f, 40f));

        left = new Rect(0, 0, 1f, screenHeight);
        right = new Rect(screenWidth - 1f, 0, 1f, screenHeight);
        top = new Rect(0, screenHeight - 1f, screenWidth, 1f);
        bottom = new Rect(0, 0, screenWidth, 1f);

        onHitDamage = transform.localScale.x + transform.localScale.y + transform.localScale.z;
        onHitDamage /= 2;
    }

    void FixedUpdate()
    {
        Vector2 currentPos = new Vector2(transform.position.x, transform.position.z);
        if(left.Contains(currentPos) || right.Contains(currentPos) || top.Contains(currentPos) || bottom.Contains(currentPos))
        {
           
        }
    }
    

}
