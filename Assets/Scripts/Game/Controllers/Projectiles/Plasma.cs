﻿using UnityEngine;

public class Plasma : Projectile {

    int counter = 0;

    public override void onCollide(RockController rock)
    {
        if (counter >= 5)
        {
            counter = 0;
            Destroy(gameObject);
        }
        counter++;
        Destroy(rock.gameObject);
    }

    public override Vector3 speed()
    {
        return transform.forward * 250f;
    }

    public override void onSpawn()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.LookAt(mousePos);
        Destroy(gameObject, 10f);
    }

    public override string ToString()
    {
        return "Plasma";
    }

}
