﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Projectile : MonoBehaviour {

    protected Rigidbody rigidBody;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        if (rigidBody != null)
        {
            onSpawn();
            rigidBody.velocity = speed();
            rigidBody.constraints = RigidbodyConstraints.FreezePositionY 
                | RigidbodyConstraints.FreezeRotationX 
                | RigidbodyConstraints.FreezeRotationY 
                | RigidbodyConstraints.FreezeRotationZ;
        }
    }
    
    void OnTriggerEnter(Collider collider)
    {
        RockController rock = collider.transform.GetComponent<RockController>();
        if (rock != null)
        {
            onCollide(rock);
        }
    }
    
    public abstract Vector3 speed();

    public abstract void onSpawn();

    public abstract void onCollide(RockController rock);

}
