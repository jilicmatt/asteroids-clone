﻿using UnityEngine;

public class Bullet : Projectile
{

    public override void onCollide(RockController rock)
    {
        Destroy(gameObject);
        Destroy(rock.gameObject);
    }

    public override void onSpawn()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.LookAt(mousePos);
        rigidBody.velocity = transform.forward * 100f;
        Destroy(gameObject, 10f);
    }

    public override Vector3 speed()
    {
        return transform.forward * 150f;
    }

    public override string ToString()
    {
        return "Bullet";
    }
}
