﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour {

    public float currentHealth, maxHealth, percentageHealth;

    public float thrust = 350f;

    private Rigidbody rigidBody;

    private Vector3 mousePos, mouseDir;

    GameHandler game;

    Camera camera;

    // Use this for initialization
    void Start ()
    {
        camera = Camera.main;
        game = GameHandler.instance;
        rigidBody = GetComponent<Rigidbody>();
        currentHealth = 100;
        maxHealth = 100;
        percentageHealth = currentHealth / maxHealth;
	}

    void FixedUpdate()
    {
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
        transform.LookAt(mousePos);

        float dist = Vector3.Distance(transform.position, mousePos);

        if (dist > 5f)
        {
            Vector3 dir = transform.position - mousePos;
            dir.y = 0;
            rigidBody.AddForce(-dir.normalized * thrust);
        }

        Fire();

    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //get object from pool
            //reset object
            //set pos
        }
        if (Input.GetButtonDown("Fire2"))
        {
            //get object from pool
            //reset object
            //set pos
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        RockController rock = collider.gameObject.GetComponent<RockController>();
        if (rock != null)
        {
            if (currentHealth > 0)
                currentHealth -= rock.onHitDamage;
            else
                currentHealth = 0;
            Destroy(rock.gameObject);
        }
    }

    public bool isDead()
    {
        return currentHealth <= 0;
    }

    public void resetVelocity()
    {
        rigidBody.velocity = Vector3.zero;
    }

    public void resetHealth()
    {
        currentHealth = maxHealth;
    }

    public void resetPosition()
    {
        transform.position = Vector3.zero;
    }
}
