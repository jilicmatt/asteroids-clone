﻿using System.Collections;
using UnityEngine;

public class SpeedBoost : PowerUp {

    public override void onCollide(Player player)
    {
        StartCoroutine(speedBoostForX(player, 10));
        Destroy(gameObject);
    }

    IEnumerator speedBoostForX(Player player, float seconds)
    {
        WrapController wrap = player.GetComponent<WrapController>();
        if (wrap != null) {
            player.thrust *= 2;
            wrap.maxSpeed *= 2;
            yield return new WaitForSeconds(seconds);
            player.thrust /= 2;
            wrap.maxSpeed /= 2;
        }
    }

}
