﻿using UnityEngine;

public class HealthPack : PowerUp {

    public override void onCollide(Player player)
    {
        player.currentHealth = player.maxHealth;
        Destroy(gameObject);
    }
}
