﻿using UnityEngine;

public abstract class PowerUp : MonoBehaviour {

    public abstract void onCollide(Player player);

    // Use this for initialization
    void Start()
    {
        BoxCollider collider = gameObject.AddComponent<BoxCollider>();
        collider.isTrigger = true;
        transform.localScale = new Vector3(10f, 10f, 10f);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material = new Material(Shader.Find("Standard"));
        renderer.material.color = Color.green;
    }

    void OnTriggerEnter(Collider collider)
    {
        Player player = collider.GetComponent<Player>();
        if (player != null)
        {
            onCollide(player);
        }
    }

}
