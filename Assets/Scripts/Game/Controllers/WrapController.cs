﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class WrapController : MonoBehaviour {

    public float maxSpeed = 100f;

    private Vector3 newPosition, viewportPosition;

    private Camera cam;
    private Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        cam = Camera.main;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        newPosition = transform.position;
        viewportPosition = cam.WorldToViewportPoint(transform.position);

        if (viewportPosition.x > 1 || viewportPosition.x < 0)
        {
            newPosition.x = Camera.main.transform.position.x - newPosition.x;
        }

        if (viewportPosition.y > 1 || viewportPosition.y < 0)
        {
            newPosition.z = Camera.main.transform.position.z - newPosition.z;
        }
        
        transform.position = newPosition;

        if (rigidbody.velocity.magnitude > maxSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
        }

    }
}
