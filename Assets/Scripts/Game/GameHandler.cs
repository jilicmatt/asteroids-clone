﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public static GameHandler instance;

    public GameObject playerPrefab;

    public Dictionary<string, Player> players = new Dictionary<string, Player>();

    private List<PowerUp> powerUps = new List<PowerUp>();

    private SceneDirector sceneDirector;
    private MainMenuManager mainMenuManager;

    public Text playerHealth;
    public const int MAX_POWER_UPS = 10;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        sceneDirector = SceneDirector.instance;
        players.Add("Player1", Instantiate(playerPrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Player>());
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            string mainMenu = "Main Menu";
            if (mainMenuManager != null) {
                sceneDirector.unloadScenes(new string[] { mainMenu });
            } else {
                sceneDirector.loadScene(mainMenu, LoadSceneMode.Additive);
            }
        }
        Player playerOne = players["Player1"];
        if(playerOne != null)
            playerHealth.text = "Health: " + playerOne.currentHealth + " / " + playerOne.maxHealth;


        foreach (Player player in players.Values)
        {
            if (player.isDead()) {
                player.resetPosition();
                player.resetHealth();
                player.resetVelocity();
            }
        }

    }

    private float nextActionTime = 0.0f;
    public float period = 10f;

    void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            SpawnPowerUp();
        }
    }

    private void SpawnPowerUp()
    {
        if (powerUps.Count < MAX_POWER_UPS)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), Camera.main.farClipPlane / 2));
            //get object from pool
            //reset object
            //set pos
        }
    }

}
