﻿using UnityEngine;
using UnityEngine.UI;

public class ResumeOrNew : MonoBehaviour {

    Text text;

    GameHandler handler;

	// Use this for initialization
	void Start () {
        text = GetComponentInChildren<Text>();
        handler = GameHandler.instance;
	}

	// Update is called once per frame
	void Update () {
        if (handler != null)
        {
            text.text = "Resume";
        } else {
            text.text = "New Game";
        }
	}
}
