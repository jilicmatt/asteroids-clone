﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    SceneDirector director;

    GameHandler handler;

    public static MainMenuManager instance;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
        director = SceneDirector.instance;
        handler = GameHandler.instance;
	}
	
    public void ToggleMusic()
    {
        
    }

    public void ResumeOrNewGame()
    {
        director.loadScene("Game", LoadSceneMode.Additive); 
        director.unloadScenes(new string[] { "Main Menu", "Credits" }); 
    }

    public void Exit()
    {
        if (handler != null)
        {
            director.unloadScenes(new string[] { "Game" });
        } else { 
            Application.Quit();
        }
    }

    public void Credits()
    {
        director.loadScene("Credits", LoadSceneMode.Additive);
        director.unloadScenes(new string[] { "Game", "Main Menu"});
    }

}
