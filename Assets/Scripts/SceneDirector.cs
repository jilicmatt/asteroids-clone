﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneDirector : MonoBehaviour {

    public static SceneDirector instance;

    public Scene parentScene;

    private Dictionary<string, AsyncOperation> loadingScenes = new Dictionary<string, AsyncOperation>();
    
    void Awake()
    {
        instance = this;
        parentScene = SceneManager.GetSceneByName("Parent Scene");
    }

    public void unloadScenes(string[] names)
    {
        foreach(string name in names)
        {
            Scene scene = SceneManager.GetSceneByName(name);
            if(scene != null) {
                if (scene.isLoaded)
                {
                    SceneManager.UnloadSceneAsync(scene);
                }
            }
        } 
    }

    // Use this for initialization
    void Start () {
        loadScene("Main Menu", LoadSceneMode.Additive);
    }

    public void loadScene(string name, LoadSceneMode mode)
    {
        if (loadingScenes.ContainsKey(name))
            return;
        Scene scene = SceneManager.GetSceneByName(name);
        if (scene != null)
        {
            if (scene.isLoaded)
                return;
            Debug.Log("Loading: " + name + ", mode: " + mode.ToString());
            StartCoroutine(load(name, mode));
        }
    }

    private IEnumerator load(string name, LoadSceneMode mode)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name, mode);

        loadingScenes.Add(name, asyncLoad);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        loadingScenes.Remove(name);
    }

    public bool areScenesLoading()
    {
        return loadingScenes.Count > 0;
    }
}
